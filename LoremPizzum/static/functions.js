$(document).ready(function(){
    $('.slider').slick({
        infinite: true,
        slidesToShow: 1,
        arrows: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000
    });
    $(".cart").mouseenter(function(){
        $(this).addClass('show');
        $(".fa-shopping-cart").addClass('show');
        $(".price").addClass('show');
    });
    $(".cart").mouseleave(function(){
        $(this).removeClass('show');
        $(".fa-shopping-cart").removeClass('show');
        $(".price").removeClass('show');
    });
});
let lang = document.querySelector('.languages');
let menu = document.querySelector('.languages-table');

lang.addEventListener('click', function(){
    lang.classList.toggle('hiden');
    menu.classList.toggle('hiden');

});