from django.shortcuts import get_object_or_404, render
from .models import Product
from cart.forms import CartAddProductForm

def index(request):
    return render(request,'index.html')

def menu(request):
  products = Product.objects.all()
  return render(request,'menu.html',{"products":products})

def product_details(request,ids):
    product = get_object_or_404(Product,id = ids)
    cart_product_form = CartAddProductForm()
    return render(request, 'product.html', {'product': product,'cart_product_form': cart_product_form})

