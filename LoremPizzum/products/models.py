from django.db import models
from django.utils.translation import gettext_lazy as _

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(_('title'),max_length = 30, null = False)
    describtion = models.CharField(_('describtion'),max_length = 150, null = False)
    price = models.FloatField(_('price'),null = False)
    img = models.ImageField(null = False, upload_to = 'media')
    
    def __str__(self):
        return self.title
